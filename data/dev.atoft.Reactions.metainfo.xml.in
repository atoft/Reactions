<?xml version="1.0" encoding="UTF-8"?>
<!-- Copyright 2021 Alastair Toft -->
<component type="desktop-application">
  <id>dev.atoft.Reactions</id>
  <metadata_license>CC0-1.0</metadata_license>
  <project_license>GPL-3.0</project_license>
  <name>Reactions</name>
  <summary>Simple GIF search.</summary>
  <description>
    <p>
      Search for the perfect GIF with Reactions!
    </p>
    <ul>
      <li>
	Search for reaction images, provided by Giphy.
      </li>
      <li>
	Drag-and-drop images to other apps, in GIF or MP4 formats.
      </li>
    </ul>
  </description>
  <screenshots>
    <screenshot type="default">
      <image>https://codeberg.org/atoft/Reactions/raw/branch/main/data/screenshots/main.png</image>
      <caption>Main window</caption>
    </screenshot>
    <screenshot>
      <image>https://codeberg.org/atoft/Reactions/raw/branch/main/data/screenshots/results.png</image>
      <caption>Search results interface</caption>
    </screenshot>
  </screenshots>
  <provides>
    <id>dev.atoft.Reactions</id>
  </provides>
  <launchable type="desktop-id">dev.atoft.Reactions.desktop</launchable>
  <url type="homepage">https://codeberg.org/atoft/Reactions/</url>
  <developer_name>Alastair Toft</developer_name>
  <content_rating type="oars-1.1" />
  <translation type="gettext">dev.atoft.Reactions</translation>
  <releases>
    <release version="0.1.0" date="2021-10-31">
      <description>
	This is the initial release of Reactions!
      </description>
    </release>
  </releases>
</component>
