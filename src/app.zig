const std = @import("std");
const c = @import("c.zig");
const gtk = @import("gtk.zig");
const ApiProvider = @import("providers/api_provider.zig").ApiProvider;
const ImageFormat = @import("image_format.zig").ImageFormat;
const ImageMetadata = @import("image_metadata.zig").ImageMetadata;
const NetSession = @import("net.zig").NetSession;
const NetMessageReceiver = @import("net.zig").NetMessageReceiver;
const MainWindow = @import("main_window.zig").MainWindow;
const Settings = @import("settings.zig").Settings;

pub const LoadedImage = struct {
    image: ImageMetadata,

    preview_receiver: NetMessageReceiver,
    full_image_receiver: NetMessageReceiver,

    preview_image: ?[]u8,
    preview_image_stream: ?*c.GInputStream,

    full_image: ?[]u8,
    full_image_stream: ?*c.GInputStream,

    file: ?*c.GFile,

    app: *App,

    pub fn init(app: *App, image: ImageMetadata) LoadedImage {
        return LoadedImage{
            .image = image,
            .preview_receiver = NetMessageReceiver.initForBytes(app.allocator, receive, handleNetErrorPreview),
            .full_image_receiver = NetMessageReceiver.initForBytes(app.allocator, receiveFull, handleNetErrorFull),
            .preview_image = null,
            .preview_image_stream = null,
            .full_image = null,
            .full_image_stream = null,
            .file = null,
            .app = app,
        };
    }

    pub fn deinit(self: *LoadedImage) void {
        if (self.preview_image_stream) |preview_image_stream| {
            c.g_object_unref(preview_image_stream);
        }

        if (self.full_image_stream) |full_image_stream| {
            c.g_object_unref(full_image_stream);
        }

        if (self.file) |file| {
            c.g_object_unref(file);
        }

        if (self.preview_image) |preview_image| {
            self.app.allocator.free(preview_image);
        }

        if (self.full_image) |full_image| {
            self.app.allocator.free(full_image);
        }

        self.image.deinit(self.app.allocator);
        std.log.info("deinit an image", .{});
    }

    pub fn requestFull(self: *LoadedImage) void {
        self.app.viewDetails(self);

        var url_string = self.app.allocator.dupeZ(u8, self.image.full_url) catch unreachable;
        defer self.app.allocator.free(url_string);

        self.app.net_session.send(url_string, &self.full_image_receiver, "image/*\x00");
    }

    fn receive(receiver: *NetMessageReceiver, message: []const u8) void {
        std.log.info("Got a stream", .{});

        var self = @fieldParentPtr(LoadedImage, "preview_receiver", receiver);

        var preview_image = self.app.allocator.dupe(u8, message) catch unreachable;

        // We download the whole image and keep it in memory, creating a stream over the bytes.
        // We could pass the received stream directly from soup, but then looping fails as we can't seek back to the start.
        var preview_image_stream = c.g_memory_input_stream_new_from_data(preview_image.ptr, @intCast(c_long, preview_image.len), null);

        self.preview_image = preview_image;
        self.preview_image_stream = preview_image_stream;

        self.app.onPreviewImageReady(self);
    }

    fn receiveFull(receiver: *NetMessageReceiver, message: []const u8) void {
        std.log.info("Got a stream for full image", .{});

        var self = @fieldParentPtr(LoadedImage, "full_image_receiver", receiver);

        var full_image = self.app.allocator.dupe(u8, message) catch unreachable;
        var full_image_stream = c.g_memory_input_stream_new_from_data(full_image.ptr, @intCast(c_long, full_image.len), null);

        self.full_image = full_image;
        self.full_image_stream = full_image_stream;

        self.app.onFullImageReady(self);

        // First, we write the gif to disk. Then we can provide it as a file over drag and drop
        // and most apps will receive it correctly.
        //
        // We also copy the file to the clipboard, and provide a couple of different formats to try and make most
        // apps receive something.

        var cache_dir = c.g_get_user_cache_dir();
        const dir_path_slice = std.mem.sliceTo(cache_dir, 0);

        var cached_file_name = std.ArrayList(u8).init(self.app.allocator);
        defer cached_file_name.deinit();
        std.fmt.format(cached_file_name.writer(), "reactions.{s}", .{@tagName(self.image.full_format)}) catch unreachable;

        var dir = std.fs.openDirAbsolute(dir_path_slice, std.fs.Dir.OpenDirOptions{}) catch |err| {
            // TODO Only needed in Zig 0.8, errorName returns sentinel terminated in master.
            const err_name = self.app.allocator.dupeZ(u8, @errorName(err)) catch unreachable;
            defer self.app.allocator.free(err_name);

            self.app.handleError("Failed to open the app cache directory.", err_name);
            return;
        };

        dir.writeFile(cached_file_name.items, full_image) catch |err| {
            // TODO Only needed in Zig 0.8, errorName returns sentinel terminated in master.
            const err_name = self.app.allocator.dupeZ(u8, @errorName(err)) catch unreachable;
            defer self.app.allocator.free(err_name);

            self.app.handleError("Failed to write the image to disk.", err_name);
            return;
        };

        var path_array_list = std.ArrayList(u8).init(self.app.allocator);
        defer path_array_list.deinit();

        // TODO Is there a clearer way to declare that this needs to be a null-terminated string?
        std.fmt.format(path_array_list.writer(), "{s}/{s}\x00", .{ dir_path_slice, cached_file_name.items }) catch unreachable;

        var gfile = c.g_file_new_for_path(path_array_list.items.ptr);

        var content_provider = c.gdk_content_provider_new_typed(c.g_file_get_type(), gfile);
        self.app.window.?.setDragAndDrop(content_provider);

        self.file = gfile;
    }

    fn handleNetErrorPreview(receiver: *NetMessageReceiver, err: [:0]const u8) void {
        var self = @fieldParentPtr(LoadedImage, "preview_receiver", receiver);
        self.handleNetError(err);
    }

    fn handleNetErrorFull(receiver: *NetMessageReceiver, err: [:0]const u8) void {
        var self = @fieldParentPtr(LoadedImage, "full_image_receiver", receiver);
        self.handleNetError(err);
    }

    fn handleNetError(self: *LoadedImage, err: [:0]const u8) void {
        const friendly_message = "Failed to load image.";

        self.app.handleError(friendly_message, err);
    }

    pub fn copyWebLink(self: LoadedImage) void {
        var display = c.gdk_display_get_default();
        var clipboard = c.gdk_display_get_clipboard(display);

        var path_string = self.app.allocator.dupeZ(u8, self.image.full_url) catch unreachable;
        defer self.app.allocator.free(path_string);

        c.gdk_clipboard_set_text(clipboard, path_string.ptr);
    }

    pub fn copyImage(self: LoadedImage) void {
        if (self.full_image) |full_image| {
            const display = c.gdk_display_get_default();
            const clipboard = c.gdk_display_get_clipboard(display);

            // Allows pasting to Nautilus.
            const gnome_copied_file_provider = blk: {
                const path = c.g_file_get_path(self.file);

                var operation_array_list = std.ArrayList(u8).init(self.app.allocator);
                defer operation_array_list.deinit();

                // TODO Is there a clearer way to declare that this needs to be a null-terminated string?
                std.fmt.format(operation_array_list.writer(), "copy\nfile://{s}\x00", .{path}) catch unreachable;

                std.log.info("{s}", .{operation_array_list.items});

                const bytes = c.g_bytes_new(operation_array_list.items.ptr, operation_array_list.items.len);
                const provider = c.gdk_content_provider_new_for_bytes("x-special/gnome-copied-files", bytes);

                break :blk provider;
            };

            // Allows pasting to LibreOffice.
            // TODO Any better options? text/uri-list doesn't seem to work.
            const g_file_provider = c.gdk_content_provider_new_typed(c.g_file_get_type(), self.file);

            var providers = [_](*c.GdkContentProvider){ g_file_provider, gnome_copied_file_provider };

            const clipboard_provider = c.gdk_content_provider_new_union(@ptrCast([*c][*c]c.GdkContentProvider, &providers), providers.len);

            _ = c.gdk_clipboard_set_content(clipboard, clipboard_provider);
        }
    }
};

pub const App = struct {
    adw_app: *c.AdwApplication,
    allocator: *std.mem.Allocator,
    net_session: NetSession,
    provider: ApiProvider,
    window: ?MainWindow,

    settings: Settings,
    type_image_format: c.GType,

    // Not especially data oriented. Need NetMessageReceivers to have a fixed address for callbacks to work.
    images: std.ArrayList(*LoadedImage),
    current_search: ?[]u8,
    search_count: i32,

    pub fn init(adw_app: *c.AdwApplication, allocator: *std.mem.Allocator) App {
        return App{
            .adw_app = adw_app,
            .allocator = allocator,
            .net_session = NetSession.init(),
            .provider = ApiProvider.init(allocator),
            .window = null,
            .settings = Settings.init(),
            .type_image_format = ImageFormat.register(),
            .images = std.ArrayList(*LoadedImage).init(allocator),
            .current_search = null,
            .search_count = 0,
        };
    }

    pub fn deinit(self: *App) void {
        self.clearResults();

        if (self.window) |*window| {
            window.deinit();
        }

        if (self.current_search) |current_search| {
            self.allocator.free(current_search);
        }
    }

    pub fn activate(self: *App) void {
        self.window = MainWindow.init(self);
        self.window.?.connectSignals();
    }

    pub fn onWindowDestroyed(self: *App) void {
        std.log.info("The GTK window was destroyed.", .{});
        if (self.window) |*window| {
            window.deinit();
        }

        self.window = null;
    }

    pub fn search(self: *App, search_string: []const u8) void {
        self.clearResults();

        if (self.current_search) |current_search| {
            self.allocator.free(current_search);
        }

        self.current_search = self.allocator.dupe(u8, search_string) catch unreachable;

        self.provider.search(&self.net_session, search_string, 0, self);

        self.search_count = 1;
    }

    pub fn loadMoreResults(self: *App) void {
        if (self.current_search) |current_search| {
            self.provider.search(&self.net_session, current_search, self.search_count, self);
            self.search_count = self.search_count + 1;
        }
    }

    pub fn addResults(self: *App, results: std.ArrayList(ImageMetadata)) void {
        for (results.items) |result| {
            var loaded_image = self.images.addOne() catch unreachable;
            loaded_image.* = self.allocator.create(LoadedImage) catch unreachable;
            loaded_image.*.* = LoadedImage.init(self, result);

            var url_string = self.allocator.dupeZ(u8, result.preview_url) catch unreachable;
            defer self.allocator.free(url_string);

            self.net_session.send(url_string, &loaded_image.*.preview_receiver, "image/*\x00");
        }
    }

    pub fn clearResults(self: *App) void {
        for (self.images.items) |image| {
            if (self.window) |*window| {
                window.clearResults();
            }

            image.deinit();
            self.allocator.destroy(image);
        }

        self.images.clearAndFree();
        self.search_count = 0;
    }

    pub fn viewDetails(self: *App, image: *LoadedImage) void {
        if (self.window) |*window| {
            window.viewDetails(image);
        }
    }

    pub fn handleError(self: *App, friendly_message: [:0]const u8, details: [:0]const u8) void {
        if (self.window) |*window| {
            window.showError(friendly_message, details);
        }
    }

    pub fn onPreviewImageReady(self: *App, image: *LoadedImage) void {
        if (self.window) |*window| {
            window.addResult(image);
        }
    }

    pub fn onFullImageReady(self: *App, image: *LoadedImage) void {
        if (self.window) |*window| {
            // TODO need to validate if it's still the same one we're focused on?
            window.showImage(image);
        }
    }
};
