const c = @import("c.zig");
const config = @import("config.zig");
const gtk = @import("gtk.zig");
const std = @import("std");
const App = @import("app.zig").App;

pub fn main() anyerror!void {
    const Allocator = std.heap.GeneralPurposeAllocator(.{});
    var allocator = Allocator{};
    var galloc = &allocator.allocator;
    defer _ = allocator.deinit();

    // Localization
    _ = c.setlocale(c.LC_ALL, "");
    _ = c.bindtextdomain(config.gettext_package, config.locale_dir);
    _ = c.bind_textdomain_codeset(config.gettext_package, "UTF-8");
    _ = c.textdomain(config.gettext_package);

    std.log.info("{s}", .{config.locale_dir});

    const adw_app = c.adw_application_new(config.app_id, c.GApplicationFlags.G_APPLICATION_HANDLES_OPEN);
    defer c.g_object_unref(adw_app);

    var reactions_app = App.init(adw_app, galloc);
    defer reactions_app.deinit();

    _ = gtk.g_signal_connect(adw_app, "activate", gtk.G_CALLBACK(activate), @ptrCast(c.gpointer, &reactions_app));
    _ = c.g_application_run(@ptrCast(*c.GApplication, adw_app), @intCast(c_int, std.os.argv.len), @ptrCast([*c][*c]u8, std.os.argv.ptr));
}

fn activate(app: [*c]c.GApplication, user_data: c.gpointer) callconv(.C) void {
    var reactions_app = @ptrCast(*App, @alignCast(@alignOf(*App), user_data));

    reactions_app.activate();
}
