const c = @import("c.zig");
const config = @import("config.zig");
const std = @import("std");
const ImageFormat = @import("image_format.zig").ImageFormat;

pub const Settings = struct {
    g_settings: *c.GSettings,

    pub fn init() Settings {
        return Settings{
            .g_settings = c.g_settings_new(config.app_id),
        };
    }

    pub fn getPreferredImageFormat(self: Settings) ImageFormat {
        const value = c.g_settings_get_enum(self.g_settings, "preferred-image-format");

        return @intToEnum(ImageFormat, @intCast(std.meta.Tag(ImageFormat), value));
    }

    pub fn setPreferredImageFormat(self: *Settings, format: ImageFormat) void {
        _ = c.g_settings_set_enum(self.g_settings, "preferred-image-format", @enumToInt(format));
    }

    pub fn setPreferredImageFormatFromInt(self: *Settings, format: anytype) void {
        _ = c.g_settings_set_enum(self.g_settings, "preferred-image-format", @intCast(c_int, format));
    }
};
